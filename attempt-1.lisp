;;; title: Attempt #1
;;;  date: 07-18-2018

(defvar cell         "A1") ;; The cell with the URL in it.
(defvar base_formula   "")
(defvar the_condition  "")
(defvar final_formula  "")

;; Remove http:// https:// and www.
(defun remove_prefixes ()
  (format nil
          "SUBSTITUTE(SUBSTITUTE(SUBSTITUTE(~a, \"www.\", \"\"), \"http://\", \"\"),\"https://\", \"\")"
          cell))

(defun find_the_slash (str &key (tail ""))
  (format nil "FIND(\"/\", ~a, 3)~a" str tail))

(defun remove_slashes (str)
  (format nil "LEFT(~a, ~a)" str (find_the_slash str :tail " - 1")))

(defun is_number (value)
  (format nil "ISNUMBER(~a)" value))

(defun myif (condition true false)
  (format nil "IF(~a, ~a, ~a)" condition true false))

;; Wrap this with that
(defun wrap (this that)
  (format nil that this))

(setq base_formula (remove_prefixes))

(setq the_condition (is_number (find_the_slash base_formula)))

(setq final_formula
      (myif the_condition (remove_slashes base_formula) base_formula))

(princ final_formula)
