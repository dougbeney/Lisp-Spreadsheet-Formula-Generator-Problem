Lisp Spreadsheet Formula Generator Problem
---

This is a little project that is [documented here](https://dougie.io/coding/lisp-formula-generator/).

The goal of this practice problem is to find the most elegant way to generate spreadsheet (Excel, Libreoffice) formulas using Lisp code.

The files are named using the convention `attempt-{number}.lisp`. The bigger the `number` the newer the attempt.
